<?php

namespace Drupal\ui_patterns_blocks;

use Drupal\block\Entity\Block;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\ui_patterns\Form\PatternDisplayFormTrait;
use Drupal\ui_patterns\UiPatternsSourceManager;
use Drupal\ui_patterns\UiPatternsManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PatternBlockFormAlter implements TrustedCallbackInterface {

  use PatternDisplayFormTrait;
  use StringTranslationTrait;

  /**
   * UI Patterns manager.
   *
   * @var \Drupal\ui_patterns\UiPatternsManager
   */
  protected $patternsManager;

  /**
   * UI Patterns manager.
   *
   * @var \Drupal\ui_patterns\UiPatternsSourceManager
   */
  protected $sourceManager;

  /**
   * Module Handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a UiPatternsBlock object.
    *
    * @param \Drupal\ui_patterns\UiPatternsManager $patterns_manager
    *   UI Patterns manager.
    * @param \Drupal\ui_patterns\UiPatternsSourceManager $source_manager
    *   UI Patterns source manager.
    * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
    *   Module handler.
    */
   public function __construct(UiPatternsManager $patterns_manager, UiPatternsSourceManager $source_manager, ModuleHandlerInterface $module_handler) {
     $this->patternsManager = $patterns_manager;
     $this->sourceManager = $source_manager;
     $this->moduleHandler = $module_handler;
   }

   /**
    * {@inheritdoc}
    */
   public static function create(ContainerInterface $container) {
     return new static(
       $container->get('plugin.manager.ui_patterns'),
       $container->get('plugin.manager.ui_patterns_source'),
       $container->get('module_handler')
     );
   }

   /**
    * {@inheritdoc}
    */
   public static function trustedCallbacks() {
     return [
       'preRenderPattern',
     ];
   }

  /**
   * Pre-render callback.
   */
  public static function preRenderPattern(array $build) {
    if (!empty($build['#id'])) {
      $block = Block::load($build['#id']);
      if ($configuration = $block->getThirdPartySettings('ui_patterns_blocks')) {
        $pattern = $configuration['pattern'] ?? NULL;
        if ($pattern && $pattern !== '_none') {
          $fields = [];
          $mapping = $configuration['pattern_mapping'][$pattern]['settings'] ?? [];

          foreach ($mapping as $source => $field) {
            if ($field['destination'] === '_hidden') {
              continue;
            }
            // Get rid of the source tag.
            $source = explode(':', $source)[1];
            $fields[$field['destination']] = $build[$source];
          }

          $element = [
            '#type' => 'pattern',
            '#id' => $pattern,
            '#fields' => $fields,
            '#context' => [
              'type' => 'block_display',
            ],
          ];

          if ($variant = $configuration['variants'][$pattern] ?? NULL) {
            $element['#variant'] = $variant;
          }

          // Add settings used by ui_patterns_settings.
          if ($pattern_settings = $configuration['pattern_settings'][$pattern] ?? NULL) {
            $element['#settings'] = $pattern_settings;
          }
          $build['content'] = $element;
        }
      }
    }
    return $build;
  }

  /**
   * Alters the block form.
   */
  public function formAlter(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\block\BlockInterface $block */
    $block = $form_state
      ->getFormObject()
      ->getEntity();
    $configuration = $block->getThirdPartySettings('ui_patterns_blocks');
    // Some modifications to make 'destination' default value work.
    $pattern = $configuration['pattern'];
    // todo: fix this.
    //if (isset($configuration['pattern_mapping'][$pattern]['settings'][$field_name][$value])) {
    //  return $configuration['pattern_mapping'][$pattern]['settings'][$field_name][$value];
    //}
    $form['third_party_settings']['#tree'] = TRUE;
    $form['third_party_settings']['ui_patterns_blocks'] = [
      '#type' => 'details',
      '#title' => $this->t('Patterns settings'),
      '#open' => TRUE,
      '#weight' => -1,
    ];
    $context = [];
    $this->buildPatternDisplayForm($form['third_party_settings']['ui_patterns_blocks'], 'blocks', $context, $configuration);
  }

  /**
   * {@inheritdoc}
   */
  protected function getDefaultValue(array $configuration, $field_name, $value) {
    // Some modifications to make 'destination' default value working.
    $pattern = $configuration['pattern'];
    if (isset($configuration['pattern_mapping'][$pattern]['settings'][$field_name][$value])) {
      return $configuration['pattern_mapping'][$pattern]['settings'][$field_name][$value];
    }
    return NULL;
  }

  /**
   * Checks if a given pattern has a corresponding value on the variants array.
   *
   * @param string $pattern
   *   Pattern ID.
   *
   * @return string|null
   *   Variant ID.
   */
  protected function getCurrentVariant($pattern) {
    $variants = $this->getSetting('variants');
    return !empty($variants) && isset($variants[$pattern]) ? $variants[$pattern] : NULL;
  }

}
