<?php

namespace Drupal\ui_patterns_blocks;

use Drupal\block\Entity\Block;
use Drupal\Core\Security\TrustedCallbackInterface;

class PatternBlockAlter implements TrustedCallbackInterface {

   /**
    * {@inheritdoc}
    */
   public static function trustedCallbacks() {
     return [
       'preRenderPattern',
     ];
   }

  /**
   * Pre-render callback.
   */
  public static function preRenderPattern(array $build) {
    if (!empty($build['#id'])) {
      $block = Block::load($build['#id']);
      if ($configuration = $block->getThirdPartySettings('ui_patterns_blocks')) {
        $pattern = $configuration['pattern'] ?? NULL;
        if ($pattern && $pattern !== '_none') {
          $fields = [];
          $mapping = $configuration['pattern_mapping'][$pattern]['settings'] ?? [];

          foreach ($mapping as $source => $field) {
            if ($field['destination'] === '_hidden') {
              continue;
            }
            // Get rid of the source tag.
            $source = explode(':', $source)[1];
            $fields[$field['destination']] = $build[$source];
          }

          $element = [
            '#type' => 'pattern',
            '#id' => $pattern,
            '#fields' => $fields,
            '#context' => [
              'type' => 'block_display',
            ],
          ];

          if ($variant = $configuration['variants'][$pattern] ?? NULL) {
            $element['#variant'] = $variant;
          }

          // Add settings used by ui_patterns_settings.
          if ($pattern_settings = $configuration['pattern_settings'][$pattern] ?? NULL) {
            $element['#settings'] = $pattern_settings;
          }
          $build['content'] = $element;
        }
      }
    }
    return $build;
  }

}
