<?php

namespace Drupal\ui_patterns_blocks\Plugin\UiPatterns\Source;

use Drupal\ui_patterns\Plugin\PatternSourceBase;

/**
 * Defines block pattern source plugin.
 *
 * @UiPatternsSource(
 *   id = "blocks",
 *   label = @Translation("Blocks"),
 *   provider = "block",
 *   tags = {
 *     "blocks"
 *   }
 * )
 */
class BlockSource extends PatternSourceBase {

  /**
   * {@inheritdoc}
   */
  public function getSourceFields() {
    return [
      $this->getSourceField('content', $this->t('Content')),
    ];
  }

}
